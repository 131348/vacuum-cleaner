#pragma once
#include "SFML\Graphics.hpp"
#include "Environment.h"
#include "StateMachine.h"
#include <random>
#include "VacReflexPercept.h"
#include "Agent.h"
#include <vector>

struct Direction {
	static const int None = 0;
	static const int Left = 1;
	static const int Right = 2;
	static const int Up = 3;
	static const int Down = 4;

	// Returns a random direction (number between 1 and 4)
	static int RandomDirection() {
		std::random_device seeder;
		std::mt19937 engine(seeder());
		std::uniform_int_distribution<int> dist(1, 4);
		return dist(engine);
	}
};

// our vacuum cleaner reflex agent
class ReflexAgent : public Agent {
private:
	sf::Vector2i position;							// position of the agent
	int direction;									// current direction the agent is moving
	bool stop;										// if true, the agent will stand still and do nothing

	Environment* environment;						// pointer to the environment agent is living in

	float performance;								// performance points for the current simulation
	int steps;										// number of steps the agent have moved the current simulation
	
	std::vector<float> performanceList;				// list of performance points for each simulation completed
	StateMachine<Agent> stateMachine;				// finite statemachine, not currently doing anything

public:
	ReflexAgent(Environment* environment);			// constructor, takes in an environment			

	void Update(Percept* p)						override;	//@ see Agent
	void Perceive(Percept* percept)				override;	//@ see Agent
	int GetDirection()							override;	//@ see Agent
	sf::Vector2i GetPosition()					override;	//@ see Agent

	void Clean();									/* Cleans the cell the agent stands on. If the entire environment is now clean,
													   stops the actors and notifies the Application that it finished cleaning*/
	void Reset(Environment* environment);			// Resets the agent and to start a new simulation iteration				
	float AveragePerformance();						// returns the average performance points over all simulations completed

	StateMachine<Agent>& GetStateMachine();			// returns the statemachine
};