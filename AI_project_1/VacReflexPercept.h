#pragma once
#include "Percept.h"

/* this class inhertis from percept and is the
   percept the reflex vacuum cleaner will receive.*/
class VacReflexPercept : public Percept {
private:
	bool dirty;			// true if the cell the  agent is standing on is dirty
	bool obstacle;		// true if the cell in front of the agent is an obstacle or wall(filler)
	bool bump;			/* true if the agent bumped into an obstacle last fram (our vacuum cleaner 
						   dont use this as it is not completly blind and can see one cell in front of itself*/
public:
	VacReflexPercept(Environment* environment, Agent* agent);		/*Takes the environment and a agent as parameters
																	  and creates corresponding percept*/
	bool SeeDirt();			// returns true if dirty;
	bool SeeObstacle();		// returns true if cell in front of agent is obstacle
	bool SeeBump();			// returns true if agent bumped into something
};