#pragma once
#include "Environment.h"
#include "Percept.h"
#include "SFML\Graphics.hpp"

class Agent {
private:
public:
	Agent(Environment* environment) {}
	virtual void Perceive(Percept* percept) = 0;	// Perceive function to be overrided by different types of agents					
	virtual void Update(Percept* p) = 0;			// Updatefunction to be overrided by different types of agents
	virtual int GetDirection() { return 0; }		// Returns the agent position
	virtual sf::Vector2i GetPosition() {			// Returns the agents direction
		return sf::Vector2i(0, 0); 
	}
};
