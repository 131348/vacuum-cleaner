#include "ReflexAgent.h"
#include <iostream>
#include <memory>
#include "Application.h"

ReflexAgent::ReflexAgent(Environment* environment) : Agent(environment), stateMachine(this) {
	this->environment = environment;
	this->position = environment->GetStartPosition();
	this->direction = Direction::RandomDirection();
	this->performance = 0;
	this->steps = 0;
	this->stop = false;	
}

void ReflexAgent::Update(Percept* p) {
	Perceive(p);
}

void ReflexAgent::Perceive(Percept* p) {
	std::shared_ptr<Percept> base(p);
	std::shared_ptr<VacReflexPercept> derived(std::static_pointer_cast<VacReflexPercept>(base));
	base.reset();
	VacReflexPercept* percept = derived.get();

	if (!this->stop) {
		this->steps++;

		if (percept->SeeObstacle()) {
			int newDir = this->direction;
			do { newDir = Direction::RandomDirection(); } while (newDir == this->direction);
			this->direction = newDir;
		}

		if (percept->SeeDirt()) {
			Clean();
		}

		else if (!percept->SeeObstacle()) {
			switch (this->direction) {
			case Direction::Left:
				this->position.x -= 1;
				break;
			case Direction::Right:
				this->position.x += 1;
				break;
			case Direction::Up:
				this->position.y -= 1;
				break;
			case Direction::Down:
				this->position.y += 1;
				break;
			default:
				std::cout << "invalid position in preceive";
				break;
			}

			std::random_device seeder;
			std::mt19937 engine(seeder());
			std::uniform_int_distribution<int> dist(1, 4);
			if (dist(engine) == 1) {
				this->direction = Direction::RandomDirection();
			}
		}
	}	
}

void ReflexAgent::Clean() {
	this->environment->GetCell(this->position).cellState = CellState::Clean;
	this->performance++;
	if (--this->environment->GetAmountOfDirt() == 0) {
		this->performanceList.push_back(this->performance / this->steps * 1000);
		this->stop = true;
		Application::iterationDone = true;
		std::cout << "Average performance: " << AveragePerformance() << std::endl;
	}
}

void ReflexAgent::Reset(Environment* environment) {
	this->environment = environment;
	this->position = environment->GetRandomPosition();
	this->direction = Direction::RandomDirection();
	this->performance = 0;
	this->steps = 0;
	this->stop = false;
}

float ReflexAgent::AveragePerformance() {
	float total = 0;
	for (auto &it : this->performanceList) {
		total += it;
	}
	float average = total / this->performanceList.size();
	return average;
}

sf::Vector2i ReflexAgent::GetPosition() {
	return this->position;
}

int ReflexAgent::GetDirection() {
	return this->direction;
}


StateMachine<Agent>& ReflexAgent::GetStateMachine() {
	return this->stateMachine;
}




