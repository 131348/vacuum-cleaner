#pragma once
#include "Environment.h"
#include <memory>


class Agent; // forward declaration

// Base class for Percepts
class Percept {
public:
	Percept(Environment* environment, Agent* agent) {}
	virtual ~Percept() {}
};
