#pragma once
#include "Environment.h"
#include "SFML\System.hpp"
#include "SFML\Window.hpp"
#include "ReflexAgent.h"

class Application {
private:	
	sf::RenderWindow window;	// The window used to render to screen
	Environment* environment;	// The enviroment the simulation takes place in
	ReflexAgent* agent;			// The agent currently living in this world

	int iterations;				// decides the number of simulation rounds to be played.

	float updateSpeed;			// Decides the speed of the simulation
	sf::Clock clock;			// stopwatch so we can keep track of time
	sf::Time elapsed;			// Stores the time since last update

	void Init(char* c);				// Intitilizes the Application, createas window etc
	void Update();				// Updates the simulation state
	void Render();				// Renders the simulation on screen

public:
	Application() { Init(nullptr); };	// default constructor, inits with a default filk
	Application(char* c) { Init(c); }	// constructor that loads fiven file, we use this with a command line arugment
	static bool iterationDone;
	~Application();
	void Run();							// Starts the Application
};