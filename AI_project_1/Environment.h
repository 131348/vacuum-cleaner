#pragma once
#include "Cell.h"
#include <vector>
#include <string>

using Cells = std::vector < std::vector<Cell> >;		

class Environment {
private:
	Cells cells;								// The grid (2d array) of cells
	Cells cellsStartState;						// The intial state of the grid, can be used to reset to the intial state
	int amountOfDirt;							// current amount of dirt in ther environment
	int dirtStartAmount;						// amount of dirt intiality in the environemnt

	bool randomizeDirt;							// if true, the environment will ignore the dirt from the input file, and randomize it instead
	const float DIRT_CHANCE = 2;				// the lower the value, the higher the concentration of dirt (if randmized) will be
	bool dynamicAgentStartPos;					// if true, the environment will give the agent a valid random starting position
	sf::Vector2i agentStartPos;					// starting position of the agent if we dont want it to be random
	bool dynamicDirt;							// if true, dirt will spawn randomly through the simulation while the agent is cleaning
	const int DIRT_SPAWN_CHANCE = 30;			// the frequency the dynmaic dirt will spawn at (lower = more frequent dirt spawns)
	
	sf::Texture dirtTexture;					// Texture used to represent the dirt
	sf::Sprite dirtSprite;						// Sprite used to represent the dirt and draw the texture on screen
	sf::RectangleShape cellRect;				// Rectangle representing the cells, we use this for drawing the cells
	const unsigned int GRID_OFFSET = 15;		// Offset positioning the Environment on screen
	const unsigned int BORDER_THICKNESS = 3;	// Borders are not represented in sfml RectangleShape so we need this to not overlapp while drawing
	
	sf::Vector2i agentPos;						// the current position of the agent in the environment

	unsigned int maxRows;						// number of rows in our grid of cells
	unsigned int maxColumns;					// number of colums in our grid of ceels

	void Setup(std::string mapdata_path, sf::Vector2u screenSize);		// Initializes the environment from a file, also takes in the size of our renderwindow

public:
	Environment(sf::Vector2u screenSize);								// takes in the screensize and calls Setup with a default file
	Environment(std::string mapdata_path, sf::Vector2u screenSize);		// takes in a filepath and a window size and calls Setup with theese parameters
	~Environment();

	void Update(sf::Vector2i agentPos);			// Update function, called each timestep of the application
	void Draw(sf::RenderWindow& window);		// Draws the environment on screen
	void Reset();								// resets the environment to the intial state, (will randomize dirt again if set to true)

	void RandomizeDirt();						// Cleans the entire grid and then selects sells at random to make dirty
	float GetCellSize();						// Return the visual size of cells
	int& GetAmountOfDirt();						// Returns the amount of dirt currently in the environment
	Cell& GetCell(sf::Vector2i pos);			// Returns the cell located at the spcified position
	sf::Vector2i GetRandomPosition();			// returns a random valid position (not obstacles, not fillers)
	sf::Vector2i GetStartPosition();			// returns the starting position speified in the setup file or a random valid location, depnding on what the application is det to
	void SpawnDirt(sf::Vector2i pos);			// Spwans dirt at the cell in given position
};