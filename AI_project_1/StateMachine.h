#pragma once
#include "State.h"

// templated statemachine
template <typename T>
class StateMachine {
private:
	State<T>* currentState;			// currently set state
	State<T>* previousState;		// the previous set state
	T* owner;						// instance of the object owning this statemachine

public:
	StateMachine<T>(T* owner) : owner(owner) {};	// Takes in the object that owns it

	void Update() {									// updates the current state, runs each timestep
		currentState->Update(owner);
	}

	void ChangeState(State<T>* newState) {			// Changes the state and sets the previous state to the state changed from
		if (currentState) {
			previousState = currentState;
			previousState->OnExit(owner);
		}
		if (newState) {
			currentState = newState;
			currentState->OnEnter(owner);
		}
	}

	void RevertState() {							// Sets the currentstate to the state we were previously at
		if (previousState) {
			currentState->OnExit(owner);
			currentState = previousState;
			currentState->OnEnter(owner);
		}
	}
};