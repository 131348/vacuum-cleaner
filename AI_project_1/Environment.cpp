#include "Environment.h"
#include <fstream>
#include <iostream>
#include <random>

Environment::Environment(sf::Vector2u screenSize) {
	Setup("grid_data.txt", screenSize);
}

Environment::Environment(std::string mapdata_path, sf::Vector2u screenSize) {
	Setup(mapdata_path, screenSize);
}

Environment::~Environment() {

}

void Environment::Update(sf::Vector2i agentPos) {
	this->agentPos = agentPos;

	if (dynamicDirt) {
		std::random_device seeder;
		std::mt19937 engine(seeder());
		std::uniform_int_distribution<int> dist(0, this->DIRT_SPAWN_CHANCE);
		int n = dist(engine);
		if (n == 0) {
			SpawnDirt(GetRandomPosition());
		}
	}
}

void Environment::Draw(sf::RenderWindow& window) {
	for (int i = 0; i < this->cells.size(); i++) {
		for (int j = 0; j < this->cells[i].size(); j++) {
			if (this->cells[i][j].cellState != CellState::Filler) {
				this->cellRect.setPosition(j * (this->cellRect.getSize().x + this->BORDER_THICKNESS) + this->GRID_OFFSET,
					i * (this->cellRect.getSize().y + this->BORDER_THICKNESS) + this->GRID_OFFSET);
				this->cells[i][j].cellState == CellState::Obstacle ?
					this->cellRect.setFillColor(sf::Color(100, 0, 0)) :
					this->cellRect.setFillColor(sf::Color::Transparent);

				if (sf::Vector2i(j, i) == this->agentPos && this->cellRect.getFillColor() == sf::Color::Transparent)
					this->cellRect.setFillColor(sf::Color::Yellow);
					
				window.draw(this->cellRect);

				if (this->cells[i][j].cellState == CellState::Dirty) {
					this->dirtSprite.setPosition(j * (this->cellRect.getSize().x + this->BORDER_THICKNESS) + this->GRID_OFFSET + this->cellRect.getSize().x / 2,
						i * (this->cellRect.getSize().y + this->BORDER_THICKNESS) + this->GRID_OFFSET + this->cellRect.getSize().y / 2);
					window.draw(this->dirtSprite);
				}
			}
		}
	}
}

float Environment::GetCellSize() {
	return this->cellRect.getSize().x + this->BORDER_THICKNESS;
}

Cell& Environment::GetCell(sf::Vector2i pos) {
	if (pos.y < this->cells.size() && pos.x < this->cells[pos.y].size()) {
		return this->cells[pos.y][pos.x];
	}
	Cell grid(CellState::Filler);
	return grid;
}

void Environment::Reset() {
	this->amountOfDirt = this->dirtStartAmount;
	this->cells = this->cellsStartState;

	if (this->randomizeDirt)
		RandomizeDirt();
}

sf::Vector2i Environment::GetRandomPosition() {
	std::random_device seeder;
	std::mt19937 engine(seeder());

	while(true) {
		std::uniform_int_distribution<int> row_dist(0, cells.size() - 1);
		int row = row_dist(engine);
		std::uniform_int_distribution<int> col_dist(0, cells[row].size() - 1);
		int col = col_dist(engine);
		if (this->cells[row][col].cellState == CellState::Clean || this->cells[row][col].cellState == CellState::Dirty) {
			return sf::Vector2i(col, row);
		}
	}

}

sf::Vector2i Environment::GetStartPosition() {
	if (this->dynamicAgentStartPos) 
		return GetRandomPosition();
	return this->agentStartPos;
}

int& Environment::GetAmountOfDirt() {
	return this->amountOfDirt;
}

void Environment::SpawnDirt(sf::Vector2i pos) {
	if (this->cells[pos.y][pos.x].cellState == CellState::Clean) {
		this->cells[pos.y][pos.x].cellState = CellState::Dirty;
		this->amountOfDirt++;
	}
}

void Environment::RandomizeDirt() {
	this->amountOfDirt = 0;
	for (auto &cellRow : cells) {
		for (auto &cell : cellRow) {
			if (!(cell.cellState == CellState::Obstacle || cell.cellState == CellState::Filler)) {
				cell.cellState = CellState::Clean;
				std::random_device seeder;
				std::mt19937 engine(seeder());
				std::uniform_int_distribution<int> dist(0, this->DIRT_CHANCE);
				int n = dist(engine);
				if (n == 0)  {
					cell.cellState = CellState::Dirty;
					this->amountOfDirt++;
				}
			}
		}
	}
	this->dirtStartAmount = this->amountOfDirt;
}

void Environment::Setup(std::string mapdata_path, sf::Vector2u screenSize) {
	this->dirtStartAmount = 0;
	this->maxRows = 0;
	this->maxColumns = 0;
	this->dynamicDirt = true;
	this->dynamicAgentStartPos = true;
	this->randomizeDirt = true;

	std::ifstream s_mapData;
	s_mapData.open(mapdata_path);
	if (!s_mapData.is_open()) {
		std::cout << "Error: Failed to open " << mapdata_path << std::endl;
		return;
	}

	std::string textline;

	while (std::getline(s_mapData, textline)) {
		if (textline[0] == 'X') {
			textline.erase(textline.begin(), textline.begin() + 1);
			this->agentStartPos.x = std::stoi(textline);
			continue;
		}
		if (textline[0] == 'Y') {
			textline.erase(textline.begin(), textline.begin() + 1);
			this->agentStartPos.y = std::stoi(textline);
			continue;
		}

		this->maxRows++;
		std::vector<Cell> newGridVector;
		auto &it = newGridVector.begin();
		Cell newCell(CellState::Filler);
		for (int i = 0; i < textline.length(); ++i) {
			switch (textline[i]) {
			case '0':
				newCell.cellState = CellState::Clean;
				newGridVector.emplace_back(newCell);
				break;
			case '1':
				newCell.cellState = CellState::Dirty;
				newGridVector.emplace_back(newCell);
				this->amountOfDirt++;
				break;
			case '2':
				newCell.cellState = CellState::Obstacle;
				newGridVector.emplace_back(newCell);
				break;

			default:
				newCell.cellState = CellState::Filler;
				newGridVector.emplace_back(newCell);
				break;
			}
			if (i + 1 > this->maxColumns)
				this->maxColumns = i + 1;
		}
		this->cells.emplace_back(newGridVector);
	}
	s_mapData.close();
	this->cellsStartState = this->cells;
	this->dirtStartAmount = this->amountOfDirt;

	if (this->randomizeDirt)
		RandomizeDirt();

	float squareWidth = (float)screenSize.x / (float)this->maxColumns;
	float squareHeight = (float)screenSize.y / (float)this->maxRows;
	squareWidth < squareHeight ?
		this->cellRect.setSize({ squareWidth - this->BORDER_THICKNESS * 2, squareWidth - this->BORDER_THICKNESS * 2 }) :
		this->cellRect.setSize({ squareHeight - this->BORDER_THICKNESS * 2, squareHeight - this->BORDER_THICKNESS * 2 });

	this->cellRect.setFillColor(sf::Color::Transparent);
	this->cellRect.setOutlineThickness(this->BORDER_THICKNESS);
	this->cellRect.setOutlineColor(sf::Color(180,180,180));

	if (!this->dirtTexture.loadFromFile("dirt.png")){
		std::cout << "Error: Failed to open texture " << mapdata_path << std::endl;
	}
	else {
		this->dirtSprite.setTexture(this->dirtTexture);
		this->dirtSprite.setOrigin(this->dirtSprite.getGlobalBounds().width / 2, this->dirtSprite.getGlobalBounds().height / 2);
		this->dirtSprite.setScale(sf::Vector2f(0.7 *this->cellRect.getSize().x / this->dirtSprite.getGlobalBounds().width,
			0.3 *this->cellRect.getSize().y / this->dirtSprite.getGlobalBounds().height));
	}
}

