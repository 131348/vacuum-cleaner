#include "Application.h"
#include <iostream>

bool Application::iterationDone = false;

Application::~Application() {
	delete this->environment;
}

void Application::Init(char* c) {
	this->window.create({ 640, 480, 32 }, "Vacuum Cleaner Simulation");
	this->updateSpeed = 900000;
	!c ? this->environment = new Environment("grid_data.txt", this->window.getSize()) :
		this->environment = new Environment(c, this->window.getSize());
	
	this->agent = new ReflexAgent(this->environment);
	this->iterations = 4;
}

void Application::Update() {
	sf::Event evt;
	while (this->window.pollEvent(evt)) {
		if (evt.type == sf::Event::Closed) {
			this->window.close();
		}
	}
	VacReflexPercept* p = new VacReflexPercept(this->environment, this->agent);
	agent->Update(p);
	environment->Update(this->agent->GetPosition());
}

void Application::Render() {
	this->window.clear(sf::Color::Black);
	this->environment->Draw(this->window);
	this->window.display();
}

void Application::Run() {
	//Init();

	for (int i = 0; i < this->iterations; i++) {
		while (this->window.isOpen() && !this->iterationDone) {
			sf::Time timestep = sf::seconds(1.0f / this->updateSpeed);
			this->elapsed += this->clock.restart();

			if (this->elapsed >= timestep) {
				Update();
				Render();
				elapsed -= timestep;
			}
			clock.restart();
		}
		this->environment->Reset(); 
		this->agent->Reset(this->environment);
		Application::iterationDone = false;
	}
	std::cout << "\n\n\n";
	system("pause");
}
