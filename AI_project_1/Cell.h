#pragma once

#include "SFML\Graphics.hpp"

enum class CellState {
	Clean,
	Dirty, 
	Filler,  // fillers represent walls and space outside the the environment, could be replaced by obstacles but this is convinient for drawing
	Obstacle
};

// simple class the represents a cell and can be evolved to include mroe information if needed.
struct Cell {
	CellState cellState;

	Cell() { this->cellState = CellState::Filler; }					// default constructor that intializes a cell to Filler
	Cell(CellState _cellState) { this->cellState = _cellState; }	// Constructor that sets the cellState to whats spesified
};