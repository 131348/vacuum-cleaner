#pragma once

// temnplated state class
template <typename T>
class State {
public:
	virtual void Update(T* entity) {}
	virtual void OnEnter(T* entity) {}
	virtual void OnExit(T* entity) {}

	virtual ~State() {}
};
