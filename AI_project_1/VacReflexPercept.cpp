#include "VacReflexPercept.h"
#include "SFML\Graphics.hpp"
#include <iostream>
#include "ReflexAgent.h"

VacReflexPercept::VacReflexPercept(Environment* environment, Agent* agent) : Percept(environment, agent) {
	this->dirty = false;
	this->obstacle = false;
	this->bump = false;

	sf::Vector2i pos = agent->GetPosition();
	sf::Vector2i view = pos;
	switch (agent->GetDirection()) {
	case Direction::Left:
		view.x -= 1;
		break;
	case Direction::Right:
		view.x += 1;
		break;
	case Direction::Up:
		view.y -= 1;
		break;
	case Direction::Down:
		view.y += 1;
		break;
	default:
		std::cout << "invalid position in preceive";
		break;
	}

	if (environment->GetCell(pos).cellState == CellState::Dirty)
		this->dirty = true;

	if (environment->GetCell(view).cellState == CellState::Obstacle || environment->GetCell(view).cellState == CellState::Filler)
		this->obstacle = true;
}

bool VacReflexPercept::SeeDirt() {
	return this->dirty;
}

bool VacReflexPercept::SeeObstacle() {
	return this->obstacle;
}

bool VacReflexPercept::SeeBump() {
	return this->bump;
}